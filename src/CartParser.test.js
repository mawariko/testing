import fs from 'fs';
import CartParser from './CartParser';

jest.mock('fs');

let parser;

beforeEach(() => {
	parser = new CartParser();
});

describe('CartParser - unit tests', () => {

	test('parseLine should return expected object', () => {
		const item = parser.parseLine('Tvoluptatem,10.32,1');
		expect(item).toEqual(expect.objectContaining({
			name: 'Tvoluptatem',
			price: 10.32,
			quantity: 1
		}));
		expect(item).toHaveProperty('id');
		expect(item.id).toEqual(expect.stringMatching(/[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}/));
	});

	test('validate should check column names', () => {
		const csv = `Wrong 1,Wrong 2,Wrong 3
		Mollis consequat,9.00,2
		Tvoluptatem,10.32,1`;
		const errors = parser.validate(csv);
		expect(errors).toHaveLength(3);
		errors.forEach((error, index) => {
			expect(error).toEqual(expect.objectContaining({
				type: parser.ErrorType.HEADER,
				row: 0,
				column: index
			}));
		});
	});

	test('validate should accept number of columns defined by schema', () => {
		const csv =
			`Product name,Price,Quantity
			Mollis consequat,9.00,2
			Tvoluptatem,10`;
		const errors = parser.validate(csv);
		expect(errors).toHaveLength(1);

		expect(errors[0]).toEqual(expect.objectContaining({
			type: parser.ErrorType.ROW,
			row: 2,
			column: -1
		}));
	});

	test('validate should not accept empty cells', () => {
		const csv =
			`Product name,Price,Quantity
			Mollis consequat,9.00,2
			,10.32,1`;
		const errors = parser.validate(csv);
		expect(errors).toHaveLength(1);
		expect(errors[0]).toEqual(expect.objectContaining({
			type: parser.ErrorType.CELL,
			row: 2,
			column: 0
		}));
	});

	test('validate should accept only positive numbers in number cells', () => {
		const csv =
			`Product name,Price,Quantity
			Mollis consequat,9.00,k
			Tvoluptatem,-10.32,1`;
		const errors = parser.validate(csv);
		expect(errors).toHaveLength(2);
		expect(errors[0]).toEqual(expect.objectContaining({
			type: parser.ErrorType.CELL,
			row: 1,
			column: 2
		}));
		expect(errors[1]).toEqual(expect.objectContaining({
			type: parser.ErrorType.CELL,
			row: 2,
			column: 1
		}));
	});

	test('validate should return 0 errors when csv is valid', () => {
		const csv = `Product name,Price,Quantity
		Mollis consequat,9.00,2
		Tvoluptatem,10.32,1`
		const errors = parser.validate(csv);
		expect(errors).toHaveLength(0);
	})

	test('calcTotal should return correct value', () => {
		const items = [{
				"id": "3e6def17-5e87-4f27-b6b8-ae78948523a9",
				"name": "Mollis consequat",
				"price": 9,
				"quantity": 2
			},
			{
				"id": "90cd22aa-8bcf-4510-a18d-ec14656d1f6a",
				"name": "Tvoluptatem",
				"price": 10.32,
				"quantity": 1
			}
		]
		const total = parser.calcTotal(items);
		expect(total).toBeCloseTo(28.32, 2);
	});

	test('createError should return correct object', () => {
		const input = ['header', 2, 1, 'this is an error message'];
		const createdError = parser.createError(...input);
		expect(createdError).toEqual({
			type: 'header',
			row: 2,
			column: 1,
			message: 'this is an error message'
		});
	});

	test('readFile shoul return content as string', () => {
		fs.readFileSync.mockImplementationOnce(() => 'test text');
		const fileContent = parser.readFile('');
		expect(fileContent).toEqual('test text');
	});

});

describe('CartParser - integration test', () => {
	test('parse should return correct object', () => {
		const csv =
			`Product name,Price,Quantity
			Mollis consequat,9.00,2
			Tvoluptatem,10.32,1`;
		fs.readFileSync.mockImplementationOnce(() => csv);
		const result = parser.parse('');

		expect(result).toHaveProperty('items');
		expect(result).toHaveProperty('total');
		expect(result.items).toBeInstanceOf(Array);
		expect(result.items).toHaveLength(2);
		expect(result.items[0]).toEqual(expect.objectContaining({
			id: expect.stringMatching(/[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}/),
			name: 'Mollis consequat',
			price: 9.00,
			quantity: 2
		}));
		expect(result.items[1]).toEqual(expect.objectContaining({
			id: expect.stringMatching(/[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}/),
			name: 'Tvoluptatem',
			price: 10.32,
			quantity: 1
		}));
		expect(result.total).toBeCloseTo(28.32, 2);
	});
});